/*
 * <one line to give the program's name and a brief idea of what it does.>
 * Copyright 2018  Joshua Christopher <email>
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as
 * published by the Free Software Foundation; either version 2 of
 * the License or (at your option) version 3 or any later version
 * accepted by the membership of KDE e.V. (or its successor approved
 * by the membership of KDE e.V.), which shall act as a proxy
 * defined in Section 14 of version 3 of the license.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef REPEATMPDEL_H
#define REPEATMPDEL_H

#include <QAbstractListModel>

#include "../data/media.h"

/**
 * @todo write docs
 */
class RepeatModel : public QAbstractTableModel
{
    Q_OBJECT

public:
    /**
     * Default constructor
     */
    RepeatModel(QObject* parent = nullptr);

    /**
     * Destructor
     */
    ~RepeatModel();

public:
    enum RepeatRoles {
        NameRole = Qt::UserRole + 1,
        StartRole,
        EndRole
    };


    int rowCount(const QModelIndex& parent = QModelIndex()) const override;
    int columnCount(const QModelIndex&parent = QModelIndex()) const override;
    QVariant data(const QModelIndex& index, int role) const override;
    QVariant headerData(int section, Qt::Orientation orientation,
                        int role = Qt::DisplayRole) const override;
    QHash<int,QByteArray> roleNames() const override;
    bool setData(const QModelIndex& index, const QVariant& value, int role = Qt::EditRole) override;
    Qt::ItemFlags flags(const QModelIndex& index) const override;

    bool insertRows(int row, int count, const QModelIndex & index) override;
    bool removeRows(int row, int count, const QModelIndex & index) override;

    int addRepeat(int a_startTime);
    void addRepeatStop(int a_stopTime,
                       int a_row);
    void removeRepeat(const QModelIndex& a_index);

    void setMedia(Media* a_media);

    void reloadData();

    Repeat* repeat(const QModelIndex& a_index) const;

private:
    Media* m_media;

};

#endif // REPEATMPDEL_H
