/*
 * <one line to give the program's name and a brief idea of what it does.>
 * Copyright 2018  Joshua Christopher <email>
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as
 * published by the Free Software Foundation; either version 2 of
 * the License or (at your option) version 3 or any later version
 * accepted by the membership of KDE e.V. (or its successor approved
 * by the membership of KDE e.V.), which shall act as a proxy
 * defined in Section 14 of version 3 of the license.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#include "repeatmodel.h"
#include <QTime>

RepeatModel::RepeatModel(QObject* parent)
    : QAbstractTableModel(parent)
{
    m_media = nullptr;
}

RepeatModel::~RepeatModel()
{
}

int RepeatModel::rowCount(const QModelIndex& parent) const
{
    Q_UNUSED(parent);
    if(m_media == nullptr)
        return 0;
    return m_media->numRepeats();
}

int RepeatModel::columnCount(const QModelIndex &parent) const
{
    return 2;
}

QVariant RepeatModel::data(const QModelIndex& index, int role) const
{
    if (!index.isValid())
        return QVariant();

    if(index.row() >= rowCount())
        return QVariant();

    if(m_media == nullptr)
        return QVariant();
    
    if(role == Qt::DisplayRole && index.column() == 0)
    {
        return Repeat::getFormat(m_media->repeat(index.row())->getStartTime());
    }
    else if(role == Qt::DisplayRole && index.column() == 1)
    {
        return Repeat::getFormat(m_media->repeat(index.row())->getEndTime());
    }
    else
    {
        return QVariant();
    }
}

QVariant RepeatModel::headerData(int section, Qt::Orientation orientation, int role) const
{
    if(role != Qt::DisplayRole)
        return QVariant();
    if(orientation == Qt::Orientation::Horizontal)
    {
        switch(section)
        {
        case 0:
            return tr("Start");
        case 1:
            return tr("Stop");
        default:
            return QVariant();
        }
    }
    else
    {
        return QVariant();
    }
}

QHash<int, QByteArray> RepeatModel::roleNames() const
{
    QHash<int,QByteArray> roles;
    roles[NameRole] = "name";
    roles[StartRole] = "start";
    roles[EndRole] = "end";
    return roles;
}

bool RepeatModel::setData(const QModelIndex& index,
                          const QVariant& value,
                          int role)
{
    if(!index.isValid() || role != Qt::EditRole)
        return false;

    if(m_media == nullptr)
        return false;
    
    bool result = false;

    if(index.column() == 0)
    {
        result = m_media->repeat(index.row())->setStartTime(value.toInt());
    }
    else if (index.column() == 1)
    {
        result = m_media->repeat(index.row())->setEndTime(value.toInt());
    }
    if (result == true)
    {
        emit dataChanged(index,index);
    }
    return result;
}

Qt::ItemFlags RepeatModel::flags(const QModelIndex& index) const
{
    if(!index.isValid())
        return 0;
    return Qt::ItemIsEditable | QAbstractItemModel::flags(index);
}

bool RepeatModel::insertRows(int position, int rows, const QModelIndex& index)
{
    Q_UNUSED(index)
    if(m_media == nullptr)
        return false;
    beginInsertRows(QModelIndex(), position, position+rows-1);
    for (int row=0; row != rows; ++row)
    {
        m_media->addRepeat(position);
    }
    endInsertRows();
    return true;
}

bool RepeatModel::removeRows(int position, int rows, const QModelIndex& index)
{
    Q_UNUSED(index)
    if(m_media == nullptr)
        return false;
    beginRemoveRows(QModelIndex(), position, position+rows-1);
    for(int row=0; row != rows; ++row)
    {
        m_media->removeRepeat(position);
    }
    endRemoveRows();
    return true;
}

int RepeatModel::addRepeat(int a_startTime)
{
    Q_ASSERT(a_startTime >= 0);
    Q_ASSERT_X(m_media != nullptr, "addRepeat", "RepeatModel has null media");
    if(m_media == nullptr)
        return -1;
    int size = m_media->numRepeats();
    insertRows(size, 1, QModelIndex());
    QModelIndex index = this->index(size, 0, QModelIndex());
    setData(index, a_startTime, Qt::EditRole);
    return size;
}

void RepeatModel::addRepeatStop(int a_stopTime, int a_row)
{
    QModelIndex index = this->index(a_row, 1);
    this->setData(index,
                  a_stopTime,
                  Qt::EditRole);
}

void RepeatModel::removeRepeat(const QModelIndex& a_index)
{
    if(!a_index.isValid())
        return;
    this->removeRows(a_index.row(),1,QModelIndex());
}

void RepeatModel::setMedia(Media* a_media)
{
    m_media = a_media;
    this->reloadData();
}

void RepeatModel::reloadData()
{
    this->beginResetModel();
    this->endResetModel();
}

Repeat* RepeatModel::repeat(const QModelIndex& a_index) const
{
    Q_ASSERT(a_index.isValid());
    Q_ASSERT(a_index.row() <= rowCount());
    Q_ASSERT(m_media != nullptr);

    return m_media->repeat(a_index.row());
}
