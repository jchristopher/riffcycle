/*
 * <one line to give the program's name and a brief idea of what it does.>
 * Copyright 2018  Joshua Christopher <email>
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as
 * published by the Free Software Foundation; either version 2 of
 * the License or (at your option) version 3 or any later version
 * accepted by the membership of KDE e.V. (or its successor approved
 * by the membership of KDE e.V.), which shall act as a proxy
 * defined in Section 14 of version 3 of the license.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#include "etudemodel.h"
#include <qt5/QtCore/qvariant.h>
#include <qt5/QtCore/QModelIndex>
#include <qt5/QtCore/qnamespace.h>

EtudeModel::EtudeModel(QObject* parent)
    : QAbstractListModel(parent)
{
    m_session = nullptr;
}

EtudeModel::~EtudeModel()
{
}

int EtudeModel::rowCount(const QModelIndex& parent) const
{
    Q_UNUSED(parent);
    if(m_session == nullptr)
        return 0;
    return m_session->numEtudes();
}

QVariant EtudeModel::data(const QModelIndex& index, int role) const
{
    if (!index.isValid())
        return QVariant();
    
    if(index.row() >= rowCount())
        return QVariant();

    if(m_session == nullptr)
        return QVariant();
    
    if(role == Qt::DisplayRole)
    {
        return m_session->etude(index.row())->name();
    }
    else
    {
        return QVariant();
    }
}

QVariant EtudeModel::headerData(int section, Qt::Orientation orientation, int role) const
{
    if(role != Qt::DisplayRole)
        return QVariant();
    if (orientation== Qt::Horizontal)
        return QStringLiteral("Column %1").arg(section);
    else
        return QStringLiteral("Row %1").arg(section);
}

QHash<int, QByteArray> EtudeModel::roleNames() const
{
    QHash<int,QByteArray> roles;
    roles[NameRole] = "name";
    roles[SizeRole] = "size";
    return roles;
} 

bool EtudeModel::setData(const QModelIndex& index,
                         const QVariant& value,
                         int role)
{
    if(m_session == nullptr)
        return false;
    if(!index.isValid() || role != Qt::EditRole)
        return false;
    if(value.toString().isEmpty())
        return false;
    bool result = m_session->setEtudeData(value.toString(), index.row());
    return result;
}

Qt::ItemFlags EtudeModel::flags(const QModelIndex& index) const
{
    if(!index.isValid())
        return 0;
    return Qt::ItemIsEditable | QAbstractItemModel::flags(index);
}

bool EtudeModel::insertRows(int position, int rows, const QModelIndex& index)
{
    Q_UNUSED(index)
    if(m_session == nullptr)
        return false;
    beginInsertRows(QModelIndex(), position, position+rows-1);
    for (int row=0; row != rows; ++row)
    {
        m_session->addEtude(position);
    }
    endInsertRows();
    return true;
}

bool EtudeModel::removeRows(int position, int rows, const QModelIndex& index)
{
    Q_UNUSED(index)
    if(m_session == nullptr)
        beginRemoveRows(QModelIndex(), position, position+rows-1);
    for(int row=0; row != rows; ++row)
    {
        m_session->removeEtude(position);
    }
    endRemoveRows();
    return true;
}

void EtudeModel::addEtude(const QString& a_name)
{
    if(m_session == nullptr)
        return;
    if(a_name.isEmpty())
        return;
    int size = m_session->numEtudes();
    insertRows(size, 1, QModelIndex());
    QModelIndex index = this->index(size, 0, QModelIndex());
    setData(index, a_name, Qt::EditRole);
}

void EtudeModel::removeEtude(const QModelIndex &a_index)
{
    if(!a_index.isValid())
        return;

    this->removeRows(a_index.row(),1,QModelIndex());
}

const Etude& EtudeModel::getEtude(const QModelIndex &a_index) const
{
    Q_ASSERT(m_session != nullptr);
    return *(m_session->etude(a_index.row()));
}

Etude& EtudeModel::getEtude(const QModelIndex &a_index)
{
    Q_ASSERT(m_session != nullptr);
    return *(m_session->etude(a_index.row()));
}

void EtudeModel::setSession(Session *a_session)
{
    m_session = a_session;
    this->reloadData();
}

void EtudeModel::reloadData()
{
    this->beginResetModel();
    this->endResetModel();
}
