// Qt headers
#include <QApplication>

// KF headers
#include <KAboutData>
#include <KLocalizedString>

#include <VLCQtCore/Common.h>
#include <VLCQtQml/QmlVideoPlayer.h>

#include "gui/mainwindow.h"

int main(int argc, char *argv[])
{
    QCoreApplication::setAttribute(Qt::AA_X11InitThreads);
    QApplication app(argc, argv);

    // Set up the VLC player
    VlcCommon::setPluginPath(app.applicationDirPath() + "/plugins");

    MainWindow* mainWindow = new MainWindow;
    mainWindow->show();

    return app.exec();
}
