#include "mediacontroller.h"
#include "ui_mediacontroller.h"

#include <QStandardPaths>
#include <QFileDialog>
#include <QItemSelectionModel>

MediaController::MediaController(QWidget *parent) :
    QWidget(parent),
    ui(new Ui::MediaController),
    m_repeatModel(new RepeatModel(this)),
    m_recentRepeat(-1),
    m_activeRepeat(nullptr)
{
    ui->setupUi(this);

    // Set up Vlc controls and controllers
    m_instance = new VlcInstance(VlcCommon::args(), this);
    m_mediaPlayer = new VlcMediaPlayer(m_instance);
    m_mediaPlayer->setVideoWidget(ui->video);

    ui->video->setMediaPlayer(m_mediaPlayer);
    ui->volume->setMediaPlayer(m_mediaPlayer);
    ui->volume->setVolume(100);
    ui->seek->setMediaPlayer(m_mediaPlayer);

    // Set up icons
    m_playIcon = QIcon::fromTheme("media-playback-start");
    m_pauseIcon = QIcon::fromTheme("media-playback-pause");

    // Set up repeat view
    ui->repeatView->setModel(m_repeatModel);
    ui->repeatView->setSelectionMode(QAbstractItemView::SingleSelection);
    ui->repeatView->setSelectionBehavior(QAbstractItemView::SelectRows);

    connect(ui->repeatView->selectionModel(),
            &QItemSelectionModel::currentRowChanged,
            this,
            &MediaController::on_repeatViewRowSelectionChanged);
    connect(m_mediaPlayer, &VlcMediaPlayer::timeChanged,
            this, &MediaController::on_mediaplayerTimeChanged);
}

MediaController::~MediaController()
{
    delete ui;
}

void MediaController::on_browseButton_clicked()
{
    QString dir = QStandardPaths::locate(QStandardPaths::HomeLocation,
                                         QString(),
                                         QStandardPaths::LocateDirectory);
    QString file = QFileDialog::getOpenFileName(this,
                                                tr("Open File"),
                                                dir);
    this->setMediaFile(file);

    m_currentMedia->setPath(QUrl(file));
}

void MediaController::on_playPauseButton_clicked()
{
    Q_ASSERT(m_media != nullptr);
    Q_ASSERT(m_mediaPlayer != nullptr);

    if(m_mediaPlayer->state() == Vlc::State::Stopped)
    {
        m_mediaPlayer->open(m_media);
    }
    m_mediaPlayer->togglePause();
    ui->playPauseButton->setIcon(getPlayPauseIcon());
}

void MediaController::on_stopButton_clicked()
{
    Q_ASSERT(m_mediaPlayer != nullptr);

    m_mediaPlayer->stop();
    checkStateChange();
}

void MediaController::on_speedupButton_clicked()
{
    Q_ASSERT(m_mediaPlayer != nullptr);

    float cur_rate = m_mediaPlayer->playbackRate();
    m_mediaPlayer->setPlaybackRate(0.1+cur_rate);
}

void MediaController::on_slowdownButton_clicked()
{
    float cur_rate = m_mediaPlayer->playbackRate();
    m_mediaPlayer->setPlaybackRate(-0.1+cur_rate);
}

void MediaController::on_startStopRepeat_clicked()
{
    int time = m_mediaPlayer->time();
    if(!m_haveFirstRepeat)
    {
        // This is first click, so set start position
        if(time == -1)
        {
            // There is no media loaded, abort
            return;
        }
        // Otherwise, create a new Repeat
        m_recentRepeat = m_repeatModel->addRepeat(time);
        ui->startStopRepeat->setText(tr("Stop repeat"));
        m_haveFirstRepeat = true;
    }
    else
    {
        Q_ASSERT_X(m_recentRepeat >= 0, "stop repeat", "Invalid repeat index");
        Q_ASSERT_X(m_recentRepeat < m_repeatModel->rowCount(),
                   "stop repeat", "Repeat index too big");
        // This is second click, so set end position
        m_repeatModel->addRepeatStop(time, m_recentRepeat);
        ui->startStopRepeat->setText(tr("Start repeat"));
        m_haveFirstRepeat = false;
    }
}

void MediaController::setMediaFile(QString a_file)
{
    m_media = new VlcMedia(a_file, true, m_instance);

    m_mediaPlayer->open(m_media);
    m_mediaPlayer->pause();
}

QIcon MediaController::getPlayPauseIcon()
{
    if(m_mediaPlayer->state() == Vlc::State::Playing)
    {
        return m_playIcon;
    }
    else
    {
        return m_pauseIcon;
    }
}

void MediaController::checkStateChange()
{
    ui->playPauseButton->setIcon(getPlayPauseIcon());
}

void MediaController::on_ChangeEtude(Etude& a_etude)
{
    // Is the media valid?
    if(a_etude.media() != nullptr)
    {
        m_currentMedia = a_etude.media();
        m_repeatModel->setMedia(m_currentMedia);
        setMediaFile(m_currentMedia->path().path());
    }
    else
    {
        // If not valid, create new media!
        m_currentMedia = new Media;
        a_etude.setMedia(m_currentMedia);
        // Stop playback too
        m_mediaPlayer->stop();
    }
}

void MediaController::on_repeatViewRowSelectionChanged(
        const QModelIndex &current,
        const QModelIndex &previous)
{
    Q_UNUSED(previous);
    m_activeRepeat = m_repeatModel->repeat(current);
    m_mediaPlayer->setTime(m_activeRepeat->getStartTime());
}

void MediaController::on_mediaplayerTimeChanged(int a_time)
{
    if(m_activeRepeat != nullptr && a_time >= m_activeRepeat->getEndTime())
    {
        m_mediaPlayer->setTime(m_activeRepeat->getStartTime());
    }
}

void MediaController::on_deleteRepeat_clicked()
{
    QModelIndex index = ui->repeatView->selectionModel()->currentIndex();
    m_repeatModel->removeRepeat(index);
}
