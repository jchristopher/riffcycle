#ifndef ETUDELISTWIDGET_H
#define ETUDELISTWIDGET_H

#include <QListView>
#include <QLineEdit>
#include <QPushButton>

#include "../models/etudemodel.h"

class EtudeListWidget : public QWidget
{
    Q_OBJECT
public:
    EtudeListWidget(QWidget* parent = nullptr);

    void initialize();
    void setSession(Session* a_session);

signals:
    void etudeSelectionChanged(Etude&);

public slots:
    void onDataLoaded();

private slots:
    void submitEtude();
    void removeEtude();
    void onSelectionChanged(const QModelIndex& a_current,
                            const QModelIndex& a_previous);

private:
    QLineEdit* m_etudeNameEntry;
    QPushButton* m_etudeSubmit;
    QPushButton* m_etudeRemove;
    QListView* m_etudeView;
    EtudeModel* m_etudeModel;
    Session* m_session;
};

#endif // ETUDELISTWIDGET_H
