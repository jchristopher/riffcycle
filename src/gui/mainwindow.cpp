#include "mainwindow.h"

#include <QStandardPaths>
#include <QToolBar>
#include <QAction>
#include <QFileDialog>
#include <QIcon>

MainWindow::MainWindow(QWidget *parent) :
    QMainWindow(parent),
    m_mediaController(new MediaController),
    m_etudeList(new EtudeListWidget),
    m_journal(nullptr),
    m_splitter(new QSplitter)
{
    m_etudeList->initialize();

    QToolBar* toolBar = new QToolBar;
    const QIcon newJournalIcon = QIcon::fromTheme("document-new");
    const QIcon openJournalIcon = QIcon::fromTheme("document-open");
    const QIcon saveJournalIcon = QIcon::fromTheme("document-save");
    const QIcon saveAsJournalIcon = QIcon::fromTheme("document-save-as");
    m_newJournalAction = new QAction(newJournalIcon,tr("New Journal"));
    m_openJournalAction = new QAction(openJournalIcon,tr("Open Journal"));
    m_saveJournalAction = new QAction(saveJournalIcon,tr("Save Journal"));
    m_saveAsJournalAction = new QAction(saveAsJournalIcon,tr("Save Journal As..."));

    // Connect actions to functions
    connect(m_newJournalAction,&QAction::triggered,
            this,&MainWindow::onNewJournal);
    connect(m_openJournalAction,&QAction::triggered,
            this,&MainWindow::onOpenJournal);
    connect(m_saveJournalAction,&QAction::triggered,
            this,&MainWindow::onSaveJournal);
    connect(m_saveAsJournalAction,&QAction::triggered,
            this,&MainWindow::onSaveAsJournal);

    toolBar->addAction(m_newJournalAction);
    toolBar->addAction(m_openJournalAction);
    toolBar->addAction(m_saveJournalAction);
    toolBar->addAction(m_saveAsJournalAction);

    this->addToolBar(toolBar);

//    QHBoxLayout* hbox = new QHBoxLayout;
//    hbox->addWidget(m_etudeList);
//    hbox->addWidget(m_mediaController);
    m_splitter->addWidget(m_etudeList);
    m_splitter->addWidget(m_mediaController);


//    QWidget *window = new QWidget();
//    window->setLayout(m_splitter);

    setCentralWidget(m_splitter);
    this->centralWidget()->setDisabled(true);
    m_saveJournalAction->setDisabled(true);
    m_saveAsJournalAction->setDisabled(true);

    // Connect etudelist signals to mediaplayer slots
    connect(m_etudeList,&EtudeListWidget::etudeSelectionChanged,
            m_mediaController,&MediaController::on_ChangeEtude);
}

MainWindow::~MainWindow()
{
    delete m_mediaController;
    delete m_etudeList;
    delete m_journal;
}

void MainWindow::onNewJournal()
{
    if(m_journal != nullptr)
    {
        delete m_journal;
    }
    m_journal = new Journal;
    m_etudeList->setSession(m_journal->session());
    this->centralWidget()->setEnabled(true);
    m_saveJournalAction->setEnabled(true);
    m_saveAsJournalAction->setEnabled(true);
}

void MainWindow::onOpenJournal()
{
    if(m_journal != nullptr)
    {
        delete m_journal;
    }

    QString dir = QStandardPaths::locate(QStandardPaths::HomeLocation,
                                         QString(),
                                         QStandardPaths::LocateDirectory);
    QUrl file = QFileDialog::getOpenFileUrl(this,
                                            tr("Open File"),
                                            QUrl::fromLocalFile(dir));
    m_journal = new Journal;
    m_journal->setPath(file);
    m_journal->loadJournal();
    this->setEnabled(true);
    m_etudeList->setSession(m_journal->session());
    // Also reload all the models
    emit dataLoaded();
    // And enable the stuffs
    this->centralWidget()->setEnabled(true);
    m_saveAsJournalAction->setEnabled(true);
    m_saveJournalAction->setEnabled(true);
    // Initialize the etudelistwidget
    m_etudeList->initialize();
}

void MainWindow::onSaveJournal()
{
    if(m_journal == nullptr)
        return;

    // If we don't already have a path, call save-as
    if(m_journal->path().isEmpty() || !m_journal->path().isValid())
    {
        this->onSaveAsJournal();
    }
    else
    {
        // All good, call the journal's save function
        m_journal->saveJournal();
    }
}

void MainWindow::onSaveAsJournal()
{
    if(m_journal == nullptr)
        return;

    // Load the save file dialog
    QString dir = QStandardPaths::locate(QStandardPaths::HomeLocation,
                                         QString(),
                                         QStandardPaths::LocateDirectory);
    QUrl file = QFileDialog::getSaveFileUrl(this,
                                            tr("Save Journal"),
                                            dir);
    m_journal->setPath(file);

    // Path set, call onSaveJournal
    this->onSaveJournal();
}
