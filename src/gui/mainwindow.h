#ifndef MAINWINDOW_H
#define MAINWINDOW_H

#include <QMainWindow>
#include <QPushButton>
#include <QSplitter>

#include "etudelistwidget.h"
#include "mediacontroller.h"

#include "../data/journal.h"

class MainWindow : public QMainWindow
{
    Q_OBJECT

public:
    explicit MainWindow(QWidget *parent = 0);
    ~MainWindow();

signals:
    void dataLoaded();

private slots:
    void onNewJournal();
    void onOpenJournal();
    void onSaveJournal();
    void onSaveAsJournal();

private:

    MediaController* m_mediaController;
    EtudeListWidget* m_etudeList;
    Journal* m_journal;
    QSplitter* m_splitter;

    QAction* m_newJournalAction;
    QAction* m_openJournalAction;
    QAction* m_saveJournalAction;
    QAction* m_saveAsJournalAction;
};

#endif // MAINWINDOW_H
