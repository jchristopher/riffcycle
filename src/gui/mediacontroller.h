#ifndef MEDIACONTROLLER_H
#define MEDIACONTROLLER_H

#include <QWidget>
#include <QIcon>

#include <VLCQtWidgets/WidgetVideo.h>
#include <VLCQtWidgets/WidgetSeek.h>
#include <VLCQtWidgets/WidgetVolumeSlider.h>
#include <VLCQtCore/Common.h>
#include <VLCQtCore/Instance.h>
#include <VLCQtCore/Media.h>
#include <VLCQtCore/MediaPlayer.h>

#include "../data/etude.h"
#include "../models/repeatmodel.h"

namespace Ui {
class MediaController;
}

class MediaController : public QWidget
{
    Q_OBJECT

public:
    explicit MediaController(QWidget *parent = 0);
    ~MediaController();

private:
    void setMediaFile(QString a_file);
    QIcon getPlayPauseIcon();
    void checkStateChange();

public slots:
    void on_ChangeEtude(Etude& a_etude);

private slots:
    void on_playPauseButton_clicked();

    void on_stopButton_clicked();

    void on_speedupButton_clicked();

    void on_slowdownButton_clicked();

    void on_browseButton_clicked();

    void on_startStopRepeat_clicked();

    void on_mediaplayerTimeChanged(int a_time);

    void on_repeatViewRowSelectionChanged(const QModelIndex &current,
                                          const QModelIndex &previous);

    void on_deleteRepeat_clicked();

private:
    Ui::MediaController *ui;
    Media* m_currentMedia;

    VlcInstance* m_instance;
    VlcMedia* m_media;
    VlcMediaPlayer* m_mediaPlayer;

    QIcon m_playIcon;
    QIcon m_pauseIcon;

    RepeatModel* m_repeatModel;
    int m_recentRepeat;
    bool m_haveFirstRepeat;

    Repeat* m_activeRepeat;
};

#endif // MEDIACONTROLLER_H
