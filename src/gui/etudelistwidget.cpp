#include "etudelistwidget.h"

#include <QIcon>
#include <QVBoxLayout>
#include <QHBoxLayout>

EtudeListWidget::EtudeListWidget(QWidget* parent)
    : QWidget(parent)
{
    m_etudeNameEntry = new QLineEdit;
    m_etudeNameEntry->setPlaceholderText(tr("Etude Name"));
    const QIcon addEtudeIcon = QIcon::fromTheme("list-add");
    m_etudeSubmit = new QPushButton(addEtudeIcon,tr("Add"),this);
    const QIcon removeEtudeIcon = QIcon::fromTheme("list-remove");
    m_etudeRemove = new QPushButton(removeEtudeIcon,tr("Remove"));
    connect(m_etudeSubmit,&QPushButton::clicked,
            this,&EtudeListWidget::submitEtude);
    connect(m_etudeRemove,&QPushButton::clicked,
            this,&EtudeListWidget::removeEtude);
    m_etudeView = new QListView;
    m_etudeModel = new EtudeModel;

    m_etudeView->setModel(m_etudeModel);

    // Connect a selection change signal to let the media player know when
    // a new etude is selected
    const QItemSelectionModel* selection = m_etudeView->selectionModel();
    connect(selection,&QItemSelectionModel::currentRowChanged,
            this,&EtudeListWidget::onSelectionChanged);



    //    m_etudeView->setCurrentIndex(startIndex);

    QVBoxLayout* vbox = new QVBoxLayout;
    QHBoxLayout* hbox = new QHBoxLayout;
    vbox->addWidget(m_etudeNameEntry);
    hbox->addWidget(m_etudeSubmit);
    hbox->addWidget(m_etudeRemove);
    vbox->addLayout(hbox);
    vbox->addWidget(m_etudeView);

    this->setLayout(vbox);
}

void EtudeListWidget::initialize()
{
    // Start by selecting the first item in the list
    QItemSelectionModel* selection = m_etudeView->selectionModel();
    const QModelIndex& startIndex = m_etudeModel->index(0);
    selection->select(startIndex,QItemSelectionModel::SelectCurrent);
    onSelectionChanged(startIndex,QModelIndex());
}

void EtudeListWidget::onDataLoaded()
{
    m_etudeModel->reloadData();
}

void EtudeListWidget::submitEtude()
{
    QString etudeName = m_etudeNameEntry->text();
    m_etudeNameEntry->clear();
    m_etudeModel->addEtude(etudeName);
}

void EtudeListWidget::removeEtude()
{
    const QModelIndex& index = m_etudeView->currentIndex();
    m_etudeModel->removeEtude(index);

}

void EtudeListWidget::onSelectionChanged(const QModelIndex &a_current, const QModelIndex &a_previous)
{
    Q_UNUSED(a_previous);

    // Let the MediaPlayer (and whoever else is listening...)
    // know that we are changing the selection
    if(m_etudeModel->rowCount() != 0)
        emit etudeSelectionChanged(m_etudeModel->getEtude(a_current));
}

void EtudeListWidget::setSession(Session *a_session)
{
    m_etudeModel->setSession(a_session);
}
