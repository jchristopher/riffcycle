/*
 * <one line to give the program's name and a brief idea of what it does.>
 * Copyright 2018  Joshua Christopher <email>
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as
 * published by the Free Software Foundation; either version 2 of
 * the License or (at your option) version 3 or any later version
 * accepted by the membership of KDE e.V. (or its successor approved
 * by the membership of KDE e.V.), which shall act as a proxy
 * defined in Section 14 of version 3 of the license.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef JOURNAL_H
#define JOURNAL_H

#include <QObject>
#include <QIODevice>

#include "session.h"

class JournalPrivate;

/**
 * @todo write docs
 */
class Journal : public QObject
{
    Q_OBJECT

public:
    Journal(QObject* parent = nullptr);
    ~Journal();
    void setName(const QString &name);
    QString name() const;

    void setSession(Session* a_session);
    const Session* session() const;
    Session* session();

    void setPath(QUrl& a_path);
    const QUrl& path() const;

    void saveJournal() const;
    void loadJournal();

signals:
    void nameChanged();

private:
    JournalPrivate* const d_ptr;
    Q_DECLARE_PRIVATE(Journal)
};

#endif // JOURNAL_H
