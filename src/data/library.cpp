/*
 * <one line to give the program's name and a brief idea of what it does.>
 * Copyright 2018  Joshua Christopher <email>
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as
 * published by the Free Software Foundation; either version 2 of
 * the License or (at your option) version 3 or any later version
 * accepted by the membership of KDE e.V. (or its successor approved
 * by the membership of KDE e.V.), which shall act as a proxy
 * defined in Section 14 of version 3 of the license.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#include "library.h"
#include "library_p.h"

Library::Library(QObject* parent)
    : QObject(parent),
      d_ptr(new LibraryPrivate())
{
}

Library::~Library()
{
    delete d_ptr;
}

int Library::numMedias() const
{
    Q_D(const Library);
    return d->m_medias.size();
}

const Media * Library::media(int a_index) const
{
    Q_D(const Library);
    return d->m_medias.at(a_index);
}

bool Library::setMediaData(const QString& a_name, int a_index)
{
    Q_D(Library);
    Media* media = d->m_medias.at(a_index);
    media->setName(a_name);
    return true;
}

bool Library::removeMedia(int a_index)
{
    Q_D(Library);
    d->m_medias.removeAt(a_index);
    return true;
}

bool Library::addMedia(int a_index)
{
    Q_D(Library);
    d->m_medias.insert(a_index, new Media(this));
    return true;
}
