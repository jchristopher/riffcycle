/*
 * <one line to give the program's name and a brief idea of what it does.>
 * Copyright 2018  Joshua Christopher <email>
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as
 * published by the Free Software Foundation; either version 2 of
 * the License or (at your option) version 3 or any later version
 * accepted by the membership of KDE e.V. (or its successor approved
 * by the membership of KDE e.V.), which shall act as a proxy
 * defined in Section 14 of version 3 of the license.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#include "journal.h"
#include "journal_p.h"

#include <QXmlStreamWriter>
#include <QXmlStreamReader>
#include <QSaveFile>
#include <QFile>

Journal::Journal(QObject* parent)
    : QObject(parent),
      d_ptr(new JournalPrivate)
{
    Q_D(Journal);
    d->m_session = new Session;
    d->m_journal_name = "JournalNameTest";
}

Journal::~Journal()
{
    Q_D(Journal);
    if(d->m_session != nullptr)
    {
        delete d->m_session;
    }
    delete d;
}

void Journal::setName(const QString& a_name)
{
    Q_D(Journal);
    d->m_journal_name = a_name;
}

QString Journal::name() const
{
    Q_D(const Journal);
    return d->m_journal_name;
}

void Journal::setSession(Session* a_session)
{
    Q_D(Journal);
    d->m_session = a_session;
}

const Session* Journal::session() const
{
    Q_D(const Journal);
    return d->m_session;
}

Session* Journal::session()
{
    Q_D(Journal);
    return d->m_session;
}

void Journal::setPath(QUrl &a_path)
{
    Q_D(Journal);
    d->m_path = a_path;
}

const QUrl& Journal::path() const
{
    Q_D(const Journal);
    return d->m_path;
}

void Journal::saveJournal() const
{
    Q_D(const Journal);
    QString filePath = d->m_path.path();
    QSaveFile* file = new QSaveFile;
    file->setFileName(filePath);
    file->open(QIODevice::WriteOnly | QIODevice::Truncate);
    QXmlStreamWriter* writer = new QXmlStreamWriter(file);
    writer->setAutoFormatting(true);
    writer->writeStartDocument();
    writer->writeStartElement("journal");
    writer->writeAttribute("name",d->m_journal_name);
    // Write children elements
    d->m_session->saveSession(writer);
    writer->writeEndElement(); // journal
    writer->writeEndDocument();
    file->commit();
}

void Journal::loadJournal()
{
    Q_D(Journal);
    QFile* file = new QFile;
    file->setFileName(d->m_path.path());
    file->open(QIODevice::ReadOnly | QIODevice::Text);
    QXmlStreamReader* reader = new QXmlStreamReader(file);
    while (!reader->atEnd())
    {
        reader->readNextStartElement();
        if(reader->isStartElement())
        {
            if(reader->name() == "journal")
            {
                // Process the journal
                QXmlStreamAttributes journalAtts = reader->attributes();
                d->m_journal_name = journalAtts.value("name").toString();
                // Now get nested elements
                d->m_session->loadSession(reader);
            }
            else
            {
                // Throw error... there should be no non-journal elements
                // at this level

            }
        }
    }
    if (reader->hasError()) {
    }
}
