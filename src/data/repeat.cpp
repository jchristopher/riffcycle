/*
 * <one line to give the program's name and a brief idea of what it does.>
 * Copyright 2018  Joshua Christopher <email>
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as
 * published by the Free Software Foundation; either version 2 of
 * the License or (at your option) version 3 or any later version
 * accepted by the membership of KDE e.V. (or its successor approved
 * by the membership of KDE e.V.), which shall act as a proxy
 * defined in Section 14 of version 3 of the license.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#include "repeat.h"
#include "repeat_p.h"

Repeat::Repeat(QObject* parent)
    : QObject(parent),
      d_ptr(new RepeatPrivate())
{
    Q_D(Repeat);
    d->m_startTime = -1;
    d->m_endTime = -1;
}

Repeat::~Repeat()
{
    delete d_ptr;
}

bool Repeat::setStartTime(int a_startTime_ms)
{
    Q_D(Repeat);
    d->m_startTime = a_startTime_ms;
    return true;
}

bool Repeat::setEndTime(int a_endTime_ms)
{
    Q_D(Repeat);
    d->m_endTime = a_endTime_ms;
    if(d->m_endTime <= d->m_startTime)
    {
        d->m_startTime = d->m_endTime;
    }
    return true;
}

int Repeat::getStartTime() const
{
    Q_D(const Repeat);
    return d->m_startTime;
}

int Repeat::getEndTime() const
{
    Q_D(const Repeat);
    return d->m_endTime;
}

QString Repeat::getFormat(int a_time_ms)
{
    if(a_time_ms < 0)
    {
        return "-";
    }

    int millis = a_time_ms % 1000;
    int seconds = (a_time_ms / 1000) % 60;
    int minutes = (a_time_ms / (1000*60)) % 60;
    int hours = (a_time_ms / (1000*60*60)) % 24;

    QString format = "";
    if(hours  > 0)
        format += QString::number(hours) + ":";
    if(minutes > 0 || !format.isEmpty())
        format += QString::number(minutes) + ":";

    format += QString::number(seconds) + "." + QString::number(millis);

    return format;
}
