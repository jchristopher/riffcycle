/*
 * <one line to give the program's name and a brief idea of what it does.>
 * Copyright 2018  Joshua Christopher <email>
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as
 * published by the Free Software Foundation; either version 2 of
 * the License or (at your option) version 3 or any later version
 * accepted by the membership of KDE e.V. (or its successor approved
 * by the membership of KDE e.V.), which shall act as a proxy
 * defined in Section 14 of version 3 of the license.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef REPEAT_H
#define REPEAT_H

#include <QObject>

class RepeatPrivate;

/**
 * @todo write docs
 */
class Repeat : public QObject
{
    Q_OBJECT
    
public:
    Repeat(QObject* parent = nullptr);
    
    ~Repeat();
    
    bool setStartTime(int a_startTime_ms);
    
    bool setEndTime(int a_endTime_ms);
    
    int getStartTime() const;
    
    int getEndTime() const;

    static QString getFormat(int a_time_ms);

private:
    RepeatPrivate* const d_ptr;
    Q_DECLARE_PRIVATE(Repeat)
};

#endif // REPEAT_H
