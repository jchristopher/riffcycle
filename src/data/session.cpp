/*
 * <one line to give the program's name and a brief idea of what it does.>
 * Copyright 2018  Joshua Christopher <email>
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as
 * published by the Free Software Foundation; either version 2 of
 * the License or (at your option) version 3 or any later version
 * accepted by the membership of KDE e.V. (or its successor approved
 * by the membership of KDE e.V.), which shall act as a proxy
 * defined in Section 14 of version 3 of the license.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#include "session.h"
#include "session_p.h"

Session::Session(QObject* parent)
    : QObject(parent),
      d_ptr(new SessionPrivate())
{
}

Session::~Session()
{
    delete d_ptr;
}

int Session::numEtudes() const
{
    Q_D(const Session);
    return d->m_etudes.count();
}

const Etude* Session::etude(int a_index) const
{
    Q_D(const Session);
    return d->m_etudes.at(a_index);
}

Etude* Session::etude(int a_index)
{
    Q_D(const Session);
    return d->m_etudes.at(a_index);
}

bool Session::setEtudeData(const QString& a_name, 
                           int a_index)
{
    Q_D(Session);
    Etude* etude = d->m_etudes.at(a_index);
    etude->setName(a_name);
    return true;
}

bool Session::removeEtude(int a_index)
{
    Q_D(Session);
    d->m_etudes.removeAt(a_index);
    return true;
}

bool Session::addEtude(int a_index)
{
    Q_D(Session);
    d->m_etudes.insert(a_index, new Etude(this));
    return true;
}

bool Session::listEtudes() const
{
    for(int i=0; i != numEtudes(); ++i)
    {
        const Etude* myetude = this->etude(i);
    }
    return true;
}

void Session::saveSession(QXmlStreamWriter* a_writer) const
{
    Q_D(const Session);
    a_writer->writeStartElement("session");

    // Loop through etudes and write each one
    QList<Etude*>::ConstIterator it = d->m_etudes.constBegin();

    for ( ; it != d->m_etudes.constEnd(); ++it ) {
        const Etude* etude = *it;
        etude->saveEtude(a_writer);
    }
    a_writer->writeEndElement(); // session
}

void Session::loadSession(QXmlStreamReader* a_reader)
{
    Q_D(Session);
    while(!a_reader->atEnd())
    {
        a_reader->readNextStartElement();
        if(a_reader->name() == "session")
        {
            // Parse the session
            // No attributes right now
            // Read and create next level
            while(!a_reader->atEnd())
            {
                a_reader->readNextStartElement();
                if(a_reader->name() == "etude")
                {
                    // Found etude
                    // Found an etude! Create it
                    int index = d->m_etudes.size();
                    Etude* etude = new Etude(this);
                    d->m_etudes.insert(index, etude);
                    // Have that etude process itself
                    etude->loadEtude(a_reader);
                }
                else
                {
                    // Found non-etude element
                    return;
                }
            }
        }
        else
        {
            // Found non-session element
            return;
        }
    }
}
