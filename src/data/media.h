/*
 * <one line to give the program's name and a brief idea of what it does.>
 * Copyright 2018  Joshua Christopher <email>
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as
 * published by the Free Software Foundation; either version 2 of
 * the License or (at your option) version 3 or any later version
 * accepted by the membership of KDE e.V. (or its successor approved
 * by the membership of KDE e.V.), which shall act as a proxy
 * defined in Section 14 of version 3 of the license.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef MEDIA_H
#define MEDIA_H

#include <QObject>
#include <QUrl>
#include <QXmlStreamWriter>
#include <QXmlStreamReader>

#include "repeat.h"

class MediaPrivate;

/**
 * @todo write docs
 */
class Media : public QObject
{
    Q_OBJECT

public:
    /**
     * Default constructor
     */
    Media(QObject* parent = nullptr);

    /**
     * Destructor
     */
    ~Media();
    
    int numRepeats() const;
    
    const Repeat* repeat(int a_index) const;
    
    Repeat* repeat(int a_index);
    
    bool removeRepeat(int a_index);
    
    bool addRepeat(int a_index);
    
    void setName(const QString& a_name);
    
    const QString& name() const;

    void setPath(const QUrl& a_path);

    const QUrl& path() const;

    void saveMedia(QXmlStreamWriter* a_writer) const;

    void loadMedia(QXmlStreamReader* a_reader);

private:
    MediaPrivate* const d_ptr;
    Q_DECLARE_PRIVATE(Media)
};

#endif // MEDIA_H
