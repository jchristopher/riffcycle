/*
 * <one line to give the program's name and a brief idea of what it does.>
 * Copyright 2018  Joshua Christopher <email>
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as
 * published by the Free Software Foundation; either version 2 of
 * the License or (at your option) version 3 or any later version
 * accepted by the membership of KDE e.V. (or its successor approved
 * by the membership of KDE e.V.), which shall act as a proxy
 * defined in Section 14 of version 3 of the license.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#include "etude.h"
#include "etude_p.h"

Etude::Etude(QObject* parent)
    : QObject(parent),
      d_ptr(new EtudePrivate())
{
    Q_D(Etude);
    d->m_media = new Media;
}

Etude::~Etude()
{
    delete d_ptr;
}

void Etude::setName(const QString& a_name)
{
    Q_D(Etude);
    d->m_name = a_name;
}

const QString& Etude::name() const
{
    Q_D(const Etude);
    return d->m_name;
}

void Etude::setMedia(Media *a_media)
{
    Q_D(Etude);
    d->m_media = a_media;
}

const Media* Etude::media() const
{
    Q_D(const Etude);
    return d->m_media;
}

Media* Etude::media()
{
    Q_D(Etude);
    return d->m_media;
}

void Etude::saveEtude(QXmlStreamWriter* a_writer) const
{
    Q_D(const Etude);
    a_writer->writeStartElement("etude");
    a_writer->writeAttribute("name",d->m_name);
    d->m_media->saveMedia(a_writer);
    a_writer->writeEndElement();
}

void Etude::loadEtude(QXmlStreamReader* a_reader)
{
    Q_D(Etude);
    // Get info for this etude, and load media
    QXmlStreamAttributes etudeAtts = a_reader->attributes();
    d->m_name = etudeAtts.value("name").toString();
    // Now read and create media
    while(!a_reader->atEnd())
    {
        a_reader->readNextStartElement();
        if(a_reader->name() == "media"
                && a_reader->isStartElement())
        {
            // Found media
            // Found a media! Create it.
            d->m_media = new Media;
            d->m_media->loadMedia(a_reader);
        }
        else if(a_reader->name() != "media")
        {
            // Found non-media element
            return;
        }
    }
}
