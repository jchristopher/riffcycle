/*
 * <one line to give the program's name and a brief idea of what it does.>
 * Copyright 2018  Joshua Christopher <email>
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as
 * published by the Free Software Foundation; either version 2 of
 * the License or (at your option) version 3 or any later version
 * accepted by the membership of KDE e.V. (or its successor approved
 * by the membership of KDE e.V.), which shall act as a proxy
 * defined in Section 14 of version 3 of the license.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef SESSION_H
#define SESSION_H

#include <QObject>
#include <QXmlStreamWriter>
#include <QXmlStreamReader>

#include "etude.h"

class SessionPrivate;

/**
 * @todo write docs
 */
class Session : public QObject
{
    Q_OBJECT

public:
    /**
     * Default constructor
     */
    Session(QObject* parent = nullptr);

    /**
     * Destructor
     */
    ~Session();

    int numEtudes() const;

    const Etude* etude(int a_index) const;

    Etude* etude(int a_index);

    bool setEtudeData(const QString& a_name,
                      int a_index);

    bool removeEtude(int a_index);
    
    bool addEtude(int a_index);
    
    bool listEtudes() const;

    void saveSession(QXmlStreamWriter* a_writer) const;

    void loadSession(QXmlStreamReader* a_reader);

private:
    SessionPrivate* const d_ptr;
    Q_DECLARE_PRIVATE(Session)
};

#endif // SESSION_H
