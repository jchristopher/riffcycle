/*
 * <one line to give the program's name and a brief idea of what it does.>
 * Copyright 2018  Joshua Christopher <email>
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as
 * published by the Free Software Foundation; either version 2 of
 * the License or (at your option) version 3 or any later version
 * accepted by the membership of KDE e.V. (or its successor approved
 * by the membership of KDE e.V.), which shall act as a proxy
 * defined in Section 14 of version 3 of the license.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#include "media.h"
#include "media_p.h"

Media::Media(QObject* parent)
    : QObject(parent),
      d_ptr(new MediaPrivate())
{
}

Media::~Media()
{
    delete d_ptr;
}

int Media::numRepeats() const
{
    Q_D(const Media);
    return d->m_repeats.size();
}

const Repeat* Media::repeat(int a_index) const
{
    Q_D(const Media);
    return d->m_repeats.at(a_index);
}

Repeat* Media::repeat(int a_index)
{
    Q_D(const Media);
    return d->m_repeats.at(a_index);
}

bool Media::removeRepeat(int a_index)
{
    Q_D(Media);
    d->m_repeats.removeAt(a_index);
    return true;
}

bool Media::addRepeat(int a_index)
{
    Q_D(Media);
    d->m_repeats.insert(a_index, new Repeat(this));
    return true;
}

void Media::setName(const QString& a_name)
{
    Q_D(Media);
    d->m_name = a_name;
}

const QString& Media::name() const
{
    Q_D(const Media);
    return d->m_name;
}

void Media::setPath(const QUrl &a_path)
{
    if(a_path.isEmpty())
        return;
    Q_D(Media);
    d->m_path = a_path;
}

const QUrl& Media::path() const
{
    Q_D(const Media);
    return d->m_path;
}

void Media::saveMedia(QXmlStreamWriter* a_writer) const
{
    Q_D(const Media);
    a_writer->writeStartElement("media");
    a_writer->writeAttribute("name",d->m_name);
    a_writer->writeAttribute("path",d->m_path.toString());
    a_writer->writeEndElement();
}

void Media::loadMedia(QXmlStreamReader *a_reader)
{
    Q_D(Media);
    // Get infor for this media
    QXmlStreamAttributes mediaAtts = a_reader->attributes();
    d->m_name = mediaAtts.value("name").toString();
    d->m_path = mediaAtts.value("path").toString();
}
